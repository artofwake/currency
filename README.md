Задача интересная.\
Затраченное время ~ 2.5 часа. Без фреймворка.\
Отразил общий каркас компонента.\
Реализовал через менеджера событий заполнение пустого хранилища при нахождении значения в последующем.\

По умолчанию, так как мы находимся в России, то курсы валют будем считать относительно рубля.\
Это условие можно учитывать при реализации методов чекеров.
Проверка наличия значения в одном из хранилищ реализовал через цепочку вызовов.

Если было бы больше времени, я бы добавил параметр в метод getRate в виде валюты, относительно которой получаем ставку.\
Добавил бы MuteCacheRateCurrencyChecker, MuteDbRateCurrencyChecker и MuteExternalRateCurrencyChecker, у которых метод getRate обернул в try catch.\
Можно было бы обернуть весь процесс в некий класс CurrencyApp.

Возможно, убрал бы передачу валюты в метод конструктура чекера.
Тогда, в метод getRate можно было бы передавать две валюты - базовую и ту, относительно которой получаем курс.

Так же можно было бы сделать ButchChecker для оптимизации запросов в бд и межсетевых запросов для получения курса по множеству валют (при условии, что нам действительно это необходимо).

Общий код для получения курса по конкретной валюте. 

```php
<?php
use artofwake\currency\entities\Currency;
use artofwake\currency\repositories\CurrencyRateRepository;
use artofwake\currency\stubs\{FileCache, DbConnection};
use artofwake\currency\checkers\{CacheRateCurrencyChecker, DbRateCurrencyChecker, ExternalRateCurrencyChecker};
use artofwake\currency\http\cbr\CbrClient;
use artofwake\currency\checkers\ChainRateCurrency;
use artofwake\currency\{CurrencyEvent, EventManager};

$em = new EventManager();
$cache = new FileCache();
$db = new DbConnection();
$httpClient = new CbrClient();
$repository = new CurrencyRateRepository($db);

$em->on(DbRateCurrencyChecker::class,
    DbRateCurrencyChecker::EVENT_FIND_RATE, function (CurrencyEvent $event) {
    $cacheRateCurrencyChecker = new CacheRateCurrencyChecker($event->getCurrency(), new FileCache());
    $cacheRateCurrencyChecker->create($event->getFindValue());
});

$em->on(ExternalRateCurrencyChecker::class,
    ExternalRateCurrencyChecker::EVENT_FIND_RATE, function (CurrencyEvent $event) use ($db){
    $dbCurrencyChecker = new CurrencyRateRepository($db);
    $dbCurrencyChecker->updateByCode($event->getCurrency()->getCode(), $event->getFindValue());
});

$currency = new Currency('USD', 'USD');
$cacheCurrency = new CacheRateCurrencyChecker($currency, $cache);
$dbCurrency = new DbRateCurrencyChecker($currency, $repository, $em);
$externalCurrency = new ExternalRateCurrencyChecker($currency, $httpClient);

$chain = new ChainRateCurrency($cacheCurrency, $dbCurrency, $externalCurrency);
$value = $chain->getRate();
```

Спасибо за внимание!