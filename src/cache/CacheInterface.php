<?php

namespace artofwake\currency\cache;

interface CacheInterface
{
    /**
     * @param string $key
     * @param callable $callable
     * @param int $time
     * @return mixed
     */
    public function getOrSet(string $key, callable $callable, int $time);

    /**
     * @param string $key
     * @param string $value
     * @param int $time
     * @return mixed
     */
    public function set(string $key, string $value, int $time);

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);
}