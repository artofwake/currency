<?php

namespace artofwake\currency;

use artofwake\currency\entities\Currency;

/**
 * Class CurrencyEvent
 * @package artofwake\currency
 *
 * @property Currency $currency
 */
class CurrencyEvent
{
    protected $sender;
    protected $currency;
    protected $findValue;

    function __construct($sender, $currency, $findValue)
    {
        $this->sender = $sender;
        $this->currency = $currency;
        $this->findValue = $findValue;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getFindValue()
    {
        return $this->findValue;
    }
}