<?php

namespace artofwake\currency\stubs;

use artofwake\currency\cache\CacheInterface;

class FileCache implements CacheInterface
{

    /**
     * @param string $key
     * @param callable $callable
     * @param int $time
     * @return mixed
     */
    public function getOrSet(string $key, callable $callable, int $time)
    {
        // TODO: Implement getOrSet() method.
    }

    /**
     * @param string $key
     * @param string $value
     * @param int $time
     * @return mixed
     */
    public function set(string $key, string $value, int $time)
    {
        // TODO: Implement set() method.
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        // TODO: Implement get() method.
    }
}