<?php

namespace artofwake\currency\checkers;

use artofwake\currency\entities\Currency;
use artofwake\currency\checkers\RateCurrencyCheckerInterface;
use DomainException;
use artofwake\currency\http\ClientInterface;

class ExternalRateCurrencyChecker implements RateCurrencyCheckerInterface
{
    CONST EVENT_FIND_RATE = 1;

    protected $currency;
    protected $httpClient;

    function __construct(Currency $currency, ClientInterface $httpClient)
    {
        $this->currency = $currency;
        $this->httpClient = $httpClient;
    }

    public function getRate() : float
    {
        $response = $this->httpClient->send();
        return $response->getRate();
    }
}