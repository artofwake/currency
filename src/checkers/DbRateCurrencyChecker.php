<?php

namespace artofwake\currency\checkers;

use artofwake\currency\CurrencyEvent;
use artofwake\currency\entities\Currency;
use artofwake\currency\checkers\RateCurrencyCheckerInterface;
use artofwake\currency\EventManager;
use artofwake\currency\repositories\CurrencyRateRepository;
use DomainException;

/**
 * Class DbRateCurrencyChecker
 * @package artofwake\checkers
 *
 * @property Currency $currency
 * @property CurrencyRateRepository $repository
 * @property EventManager|null $em
 */
class DbRateCurrencyChecker implements RateCurrencyCheckerInterface
{
    const EVENT_FIND_RATE = 1;

    protected $currency;
    protected $repository;
    protected $em;

    /**
     * DbRateCurrencyChecker constructor.
     * @param Currency $currency
     * @param CurrencyRateRepository $repository
     * @param EventManager|null $em
     */
    function __construct(Currency $currency, CurrencyRateRepository $repository, ?EventManager $em)
    {
        $this->currency = $currency;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function getRate() : float
    {
        $currency = $this->repository->getByCode( $this->currency->getCode() );
        if (!$currency) {
            throw new DomainException("Currency {$this->currency->getName()} not found");
        }

        $value = $currency->getValue();
        if ($this->em) {
            $this->em->trigger(
                self::class,
                self::EVENT_FIND_RATE,
                new CurrencyEvent($this, $this->currency, $value)
            );
        }
        return $value;
    }
}