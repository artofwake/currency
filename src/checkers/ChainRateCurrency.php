<?php

namespace artofwake\currency\checkers;

use artofwake\currency\checkers\RateCurrencyCheckerInterface;

class ChainRateCurrency implements RateCurrencyCheckerInterface
{
    protected $checkers;

    function __construct(RateCurrencyCheckerInterface ...$checkers)
    {
        $this->checkers = $checkers;
    }

    public function getRate() : float
    {
        foreach ($this->checkers as $checker) {
            if (!is_null($value = $checker->getRate())) {
                return $value;
            }
        }
    }
}