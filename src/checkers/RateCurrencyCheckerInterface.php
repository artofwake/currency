<?php

namespace artofwake\currency\checkers;

interface RateCurrencyCheckerInterface
{
    public function getRate() : float;
}