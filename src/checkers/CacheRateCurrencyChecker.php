<?php

namespace artofwake\currency\checkers;

use artofwake\currency\cache\CacheInterface;
use artofwake\currency\entities\Currency;
use artofwake\currency\checkers\RateCurrencyCheckerInterface;

/**
 * Class CacheCurrency
 * @package artofwake\cache
 *
 * @property Currency $currency
 * @property int $ttl
 */
class CacheRateCurrencyChecker implements RateCurrencyCheckerInterface
{
    protected $currency;
    protected $cache;
    protected $ttl;

    function __construct(Currency $currency, CacheInterface $cache, $ttl = 60)
    {
        $this->currency = $currency;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }

    /**
     * @return string
     */
    protected function getKey() : string
    {
        return $this->currency->getCode();
    }

    public function create($value)
    {
        $this->cache->set($this->getKey(), $value, $this->ttl);
    }

    /**
     * @return float
     */
    public function getRate() : float
    {
        return $this->cache->getOrSet($this->getKey(), function () {
            return $this->currency->getValue();
        }, $this->ttl);
    }
}