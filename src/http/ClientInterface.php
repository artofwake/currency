<?php

namespace artofwake\currency\http;

interface ClientInterface
{
    public function send() : ResponseInterface;
}