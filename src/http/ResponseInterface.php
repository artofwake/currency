<?php

namespace artofwake\currency\http;

interface ResponseInterface
{
    public function getRate();
}