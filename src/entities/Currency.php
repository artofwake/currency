<?php

namespace artofwake\currency\entities;

/**
 * Class Currency
 * @package artofwake\currency\entities
 *
 * @property string $code
 * @property string $name
 * @property int|float $value
 */
class Currency
{
    protected $code;
    protected $name;
    protected $value;

    function __construct(string $name, string $code)
    {
        $this->name = $name;
        $this->code = $code;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}