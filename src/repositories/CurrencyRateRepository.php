<?php

namespace artofwake\currency\repositories;

use artofwake\currency\entities\Currency;
use artofwake\currency\stubs\DbConnection;

/**
 * Class CurrencyRepository
 * @package artofwake\repositories
 */
class CurrencyRateRepository
{
    protected $db;

    function __construct(DbConnection $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $code
     * @return Currency|null
     */
    public function getByCode(string $code) : ?Currency
    {
        // perform
    }

    /**
     * @param string $code
     * @param float $value
     */
    public function updateByCode(string $code, float $value) : void
    {
        // perform
    }
}